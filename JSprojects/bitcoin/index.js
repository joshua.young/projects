//////////////////////////////////////////////////////////////////////////
//                BITCOIN CURRENT PRICE CONVERSION TOOL
//
// This program allows the user to access the current price of Bitcoin in
// three different currencies, the US Dollar, the British Pound, and the
// European Euro.
// This program utilizes the endpoint from the coindesk.com
// API to access current pricing data. The data is sent to the client in
// JSON format, it is parsed server side and then the queried currency
// pricing is then sent and displayed via the home.ejs file (via the header
// and footer partial .ejs files.)

// Files in program include: index.ejs, home.ejs, footer.ejs, header.ejs,
// styles.css, package-lock.json, package.json

// Index.js is includes >>
// required import packages that enable client server communication and
// routing methods. Additionally, connection to the endpoint APIs and
// server side methods are found.

// Author: Joshua Young
// Date: 06 May 2023
// Contact: joshua.young@austincc.edu

/////////////////////////////////////////////////////////////////////////
//         SERVER SECTION

// Import express to enable program to be able to create HTTP routes,
// handle requests and responses
const express = require("express");

// Assign express to app object to enable access and use of express
// Enable access to express routing functionality
const app = express();

// Retrieves port setup in Node.js environment
// Or if .PORT is null,  port 3000 is assigned to port
// const port is used in routing express and listening for incoming requests
const port = process.env.PORT || 3000;

// Imports npm morgan to log all middleware activity to terminal
// Currently the logger is set to "dev" to enable development debugging
const logger = require("morgan");
app.use(logger("dev"));

// Enables express to look into public folder and
// use static files such as stylesheets and any images
// Create a set file path for the static files
app.use(express.static("public"));

// Parses any incoming HTML code and adds it to the req.body to be used
// server side - This is used specifically in the "app.get('/getprice')"
// to get which currency type/price the client has requested
app.use(express.urlencoded({ extended: true }));
// Parses any incoming json and adds it to the req.body to be used
// // server side - This is used specifically in the "app.get('/getprice')"
// to parse the json sent from Coindesk api. Data parsed and added
// to req.body include the needed currency type and associated price
app.use(express.json());

// Root route is defined as "/"
// When root is called, the client is redirected to the home route "/home"
app.get("/", (req, res) => res.redirect("/home"));

// Home route is defined and when called by client 'home.ejs' will render
// Of note: home.ejs will initially render 'curPrice' as empty, as the
// client will not yet have selected the currency of choice to convert
// If curPrice not provided any value, an error will occur as the program
// expects a value.
app.get("/home", (req, res) => res.render("home.ejs", { curPrice: "" }));

// Function to listen for requests at the assigned port (port variable)
// `Bitcoin ${port}`is printed to the console when request is made at
// the assigned port to confirm that the server is running and working
app.listen(port, () => console.log(`Bitcoin port ${port}`));

// Define the base URL for the Coindesk API
// Allows for flexibility in adding additional endpoints and routes
// Improves code readability by separating the base URL from endpoint URLs
const baseURL = "https://api.coindesk.com/";

// Define the route for the GET request to retrieve Bitcoin price
// The route is '/getPrice' and is used to make a query request
// JSON data received from API is parsed for use in conditional logic
// Uses switch/case conditional logic to determine requested currency type
// Returns applicable parsed currency data
app.get("/getPrice", (req, res) => {
  // Route for current price API endpoint is defined
  let route = "v1/bpi/currentprice.json";

  // Combined baseURL and specified endpoint route to be used to
  // fetch currency data from API
  let endpoint = `${baseURL}/${route}`;

  // Assigns client query data request
  // The data is the currency type being requested:
  // (US Dollar (USD), British Pound (GBP), and European Euro (EUR))
  // Variable data will be used in conditional logic to determined
  // what parsed data to return
  let { bitcoin } = req.query;
  //console.log(bitcoin);

  // Fetch function requests data from API using the Coindesk API
  // Data returned is passed as the response (in the next chained function)
  // to be parsed. Error thrown if the route is broken.
  fetch(endpoint)
    .then((response) => {
      if (response.ok) {
        return response.json();
      } else {
        throw Error("This is broke!");
      }
    })
    // Parsed response is returned and passed as usable data in
    // the next chained function to be used in Switch/Case
    // conditional logic.
    .then((data) => {
      //console.log(data);

      //curPrice initialized without value to be used to return
      // the requested string currency value object to .EJS file
      let curPrice;
      // Conditional logic used to select currency type price to return
      // Returned currency rate is formatted into applicable number format
      // for readability on the front-end.
      switch (bitcoin) {
        case "USD":
          console.log(data.bpi.USD.rate_float);
          curPrice = new Intl.NumberFormat("en-US", {
            style: "currency",
            currency: "USD",
          }).format(data.bpi.USD.rate_float);
          break;
        case "GBP":
          console.log(data.bpi.GBP.rate_float);
          curPrice = new Intl.NumberFormat("en-GB", {
            style: "currency",
            currency: "GBP",
          }).format(data.bpi.GBP.rate_float);
          break;
        case "EUR":
          console.log(data.bpi.EUR.rate_float);
          curPrice = new Intl.NumberFormat("en-EU", {
            style: "currency",
            currency: "EUR",
          }).format(data.bpi.EUR.rate_float);
          break;
      }
      console.log(curPrice);
      //res.send(curPrice);

      // home.ejs is rendered again, this time with curPrice having
      // an assigned value.
      res.render("home.ejs", { curPrice });
    })
    .catch((err) => {
      console.log(err);
      //res.send("There is an error!");
    });
});
