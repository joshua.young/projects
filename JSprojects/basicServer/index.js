//Foundation

//Allowing access to express so we can use it
const express = require("express");

//Create an instance of this application
const app = express();

//Not necessary, but is useful to check on the process
//console.log(process.env);

//Assigns the PORT to be used
const port = process.env.PORT || 3000;

//Route Handlers  -"/" by itself refers to the root route
app.get("/", (req, res) => res.send("My first server"));

app.get("/sales", (req, res) => {
  res.send("We have a lot of sales");
});

app.get("/cage", (req, res) => {
  res.redirect(
    "https://www.esquire.com/entertainment/movies/a30763531/nicolas-cage-the-unbearable-weight-of-massive-talent-movie-details-release-date-plot-cast/"
  );
});

//The star route
app.get("*", (req, res) => {
  res.send("Error 404");
});

//app.get("/sales", (req, res) => {res.send("We have a lot of sales");});
//Listener - sets up to see if anyone accesses the PORT
app.listen(port, () => console.log(`Basic Server on port ${port}`));
