const express = require("express");

const app = express();

const port = process.env.PORT || 3000;

const logger = require("morgan");
app.use(logger("dev"));

app.get("/", (req, res) => res.send("Root Route"));

app.get("/", (req, res) => res.redirect("/home"));

app.get("/home", (req, res) => res.render("home.ejs"));

app.listen(port, () => console.log(`EJS Form port ${port}`));
