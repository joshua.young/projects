const express = require("express");

const app = express();

const port = process.env.PORT || 3000;

const logger = require("morgan");
app.use(logger("dev"));

//Enables express to look into public folder and use stylesheets
app.use(express.static("public"));

//body-parser
app.use(express.urlencoded({ extended: true }));
app.use(express.json());

app.get("/", (req, res) => res.redirect("/home"));

app.get("/home", (req, res) => res.render("home.ejs"));

app.get("/error", (req, res) => res.render("error.ejs"));

app.get("/getImage", (req, res) => {
  res.send("You clicked me");
});

app.listen(port, () => console.log(`Doggy Port at ${port}`));
