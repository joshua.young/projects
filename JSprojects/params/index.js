//Import server express
const express = require("express");

//Assign express to variable
const app = express();

//Initialize and assign port
const port = process.env.PORT || 3000;

//Initialize Looger
const logger = require("morgan");
app.use(logger("dev"));

//Route Handler
//Redirect
app.get("/", (req, res) => res.redirect("/home"));

app.get("/home", (req, res) => res.send("I am the home route"));

app.get("/fruits", (req, res) => {
  res.send("I am the fruit page");
});

//params
app.get("/:fruits", (req, res) => {
  //req object with params property
  //params is an object with fruits as a key
  res.send(`I am the ${req.params.fruits} page`);
});

app.get("/fruits/:fuzzy", (req, res) => {
  res.send(`I am the ${req.params.fuzzy} site`);
});

app.get("/fruits/:fruits/eat/:love", (req, res) => {
  const { fruits, love } = req.params;
  res.send(`I ${req.params.love} ${req.params.fruits}`);
});

//build a route that matches /name/:name/bank/:money
//send a message with name and dollar amount from money
//if money is less than 100 - "{name}, Do you like living on the edge with your {money} dollars?"
//if more 100 "{name}, can I borrow {half of money}"

app.get("/name/:name/bank/:money", (req, res) => {
  let { name, money } = req.params;

  money = Number(money);

  let result =
    money < 100
      ? `${name}, Do you like living on the edge with your $${money} dollars?`
      : res.send(`${name}, can I borrow $${money / 2}`);

  res.send(result);

  //   if (Number(money) <= 100) {
  //     res.send(
  //       `${name}, Do you like living on the edge with your $${money} dollars?`
  //     );
  //   } else {
  //     money = Number(money) / 2;
  //     res.send(`${name}, can I borrow $${money}`);
  //   }
});

//App listener
app.listen(port, () => console.log(`This is the param server on port ${port}`));
