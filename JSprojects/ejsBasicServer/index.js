const express = require("express");

const app = express();

const port = process.env.PORT || 3000;

//Initialize Looger
const logger = require("morgan");
app.use(logger("dev"));

app.get("/", (req, res) => res.redirect("/home"));

app.get("/home", (req, res) => {
  let data = { dog: "woof", mouse: "squeakers" };

  //first arg = file you want rendered
  //second arg = data
  res.render("home.ejs", { data: data });
});

app.listen(port, () => console.log(`EJS server ${port}`));
