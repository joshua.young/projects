//Pokemon Battle
//Create Pokedex and program pokemon battles

/////Import express to establish server including routing procedures//////////////////////////////////////////////

//Import express to enable program to be able to create HTTP routes, handle requests and responses
const express = require("express");

//Assigning express to app object to enable access and use of express throughout code
//Enable access to express routing functionality
const app = express();

//Retrieves port setup in Node.js environment
//Or if .PORT is null,  port 3000 is assigned to port
//const port is used in routing express and listening for incoming requests
const port = process.env.PORT || 3000;

//Root route is defined as "/"
//When a Get request is made to the root route
//"I am the Pokemon root route" is sent as a response
app.get("/", (req, res) => res.send("I am the Pokemon root route"));

//function to listen for requests at the assigned port (port variable)
//`Pokemon server on port ${port}`is printed to the console when request is made at the assigned port
//This confirms that the server is running and working
app.listen(port, () => console.log(`Pokemon server on port ${port}`));

/////Import Pokemon package and utilize pokemon package with Server///

//Import the Pokemon npm package includes Pokemon characters
const pokemon = require("pokemon");

//Route /pokemon is defined
//When get request is made to /pokemon
//The name of a random Pokemon is sent in response
app.get("/pokemon", (req, res) => res.send(pokemon.random()));

//Route /dex is defined
//When get request is made to /dex
//The Pokedex with array of pokemon sent in response
app.get("/dex", (req, res) => res.send(dexArray));

//Route /battle is defined
//When get request is made to /battle
//Battle will call the battleResult function, pass through random pokemon from the dexArray
//compare the difference of the two battle pokemon attacks and defense and assign a winner and looser
//Function will return battle results and will send to client
//Passing random pokemon will allow the client to refresh the page and see the battle results of multiple pokemon matches
//Random Integers that can be generated are 0 through 4 (inclusive) to account for all 5 pokemon in the pokedex
app.get("/battle", (req, res) => {
  res.send(
    battleResult(
      dexArray[Math.floor(Math.random() * 5)],
      dexArray[Math.floor(Math.random() * 5)]
    )
  );
});

//Initialize PokeDex Array and battle functionality///////////////////////////////////////////////////////////////

//Assign five pokemon names to poke variables
//Reuse in dex and in battles
let pokeOne = pokemon.getName(1);
let pokeTwo = pokemon.getName(2);
let pokeThree = pokemon.getName(3);
let pokeFour = pokemon.getName(4);
let pokeFive = pokemon.getName(5);

//Create Pokedex

//Function Returns random attack between the min and max parameters
let randAttack = (min, max) => {
  return Math.floor(Math.random() * (max - min)) + min;
};

//Function Returns random defense between the min and max parameters
let randDefense = (min, max) => {
  return Math.floor(Math.random() * (max - min)) + min;
};

//PokeDex assigned pokemon using poke variables and random number for the attack and defense
//Attack min: 50, max 100
//Defense max: 0, max 100
let dexArray = [
  {
    Pokemon: pokeOne,
    Attack: randAttack(50, 100),
    Defense: randDefense(0, 100),
  },
  {
    Pokemon: pokeTwo,
    Attack: randAttack(50, 100),
    Defense: randDefense(0, 100),
  },
  {
    Pokemon: pokeThree,
    Attack: randAttack(50, 100),
    Defense: randDefense(0, 100),
  },
  {
    Pokemon: pokeFour,
    Attack: randAttack(50, 100),
    Defense: randDefense(0, 100),
  },
  {
    Pokemon: pokeFive,
    Attack: randAttack(50, 100),
    Defense: randDefense(0, 100),
  },
];

//BattleResult is initialized and assigned a value from the associated fat arrow function
//Two Pokemon are passed through and their attacks and defense points are compared
//The difference of the first pokemon's attack - the second pokemon's defense must be
//greater than the resulting difference of the seconds pokemon's attack versus the first's defense.
//In other words, whichever Pokemon has the greater difference wins.

let battleResult = (pokemonOne, pokemonTwo) => {
  //difOne and DifTwo and the following two if statements are to ensure that
  //there are no negative attack/defense differences
  //If the difference is negative, the result will be assigned a 0.
  //Otherwise, the resulting difference will be left unchanged
  let difOne;
  let difTwo;

  if (pokemonOne.Attack - pokemonTwo.Defense > 0) {
    difOne = pokemonOne.Attack - pokemonTwo.Defense;
  } else {
    difOne = 0;
  }

  if (pokemonTwo.Attack - pokemonOne.Defense > 0) {
    difTwo = pokemonTwo.Attack - pokemonOne.Defense;
  } else {
    difTwo = 0;
  }

  //Differences are compared and the results are returned
  if (difOne > difTwo) {
    return `${pokemonOne.Pokemon} wins with an attack of ${pokemonOne.Attack} and defense of ${pokemonOne.Defense}. 
  ${pokemonTwo.Pokemon} loses with an attack of ${pokemonTwo.Attack} and defense of ${pokemonTwo.Defense}.`;
  } else if (difOne < difTwo) {
    return `${pokemonTwo.Pokemon} wins with an attack of ${pokemonTwo.Attack} and defense of ${pokemonTwo.Defense}. 
  ${pokemonOne.Pokemon} loses with an attack of ${pokemonOne.Attack} and defense of ${pokemonOne.Defense}.`;
  } else {
    return `Woah! This is a rare case, but these Pokemon just battled for a tie! ${pokemonTwo.Pokemon} with an attack of ${pokemonTwo.Attack} and defense of ${pokemonTwo.Defense}. 
  ${pokemonOne.Pokemon} with an attack of ${pokemonOne.Attack} and defense of ${pokemonOne.Defense}.`;
  }
};
