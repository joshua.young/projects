//Import Animals npm package
var animals = require("animals");
//Import log.pets package
var pet = require("log.pets");

//Displays a random animal from Animals array
console.log(`Animal: ${animals()}`);

//Displays an ASCII Lion
console.log(`Lion: ${pet.lion()}`);

//Displays zoo with list of randomly generated animal names
console.log(pet.zoo(animals(), animals(), animals()));

//Displays the number of possible animals generated
console.log(`# of animals in generator: ${animals.words.length}`);

//Filters animals array for all animals that start with G.  Return temp array and assign to variable.
const gAnimals = animals.words.filter((el) => {
  return el.toLowerCase().startsWith("g");
});

//Display the length of the temp array for gAnimals = number of animals that start with G.
console.log(`# of animals starting with G: ${gAnimals.length}`);
