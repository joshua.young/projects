//Import readline-sync npm package
var input = require("readline-sync");

//Calls function to request food spice level.
//Returns confirmation index to pass to displayOrder
let confirmIndex = spiceChoice();

//test
//console.log(confirmIndex);

//Calls displayOrder function and passes confirmIndex to display the final result
displayOrder(confirmIndex);

function spiceChoice() {
  //Taco Spice level options array
  let spiceLevel = [
    "spicy",
    "very spicy",
    "so spicy, you won't be able to feel your face",
  ];

  //Yes or No response array
  let yesOrNo = ["Yes", "No"];

  //Displays a question to user,
  //Requires integer as use input,
  //Assigns integer as the array index
  let index = input.keyInSelect(
    spiceLevel,
    "How spicy would you like your tacos? "
  );

  //Requires user to confirm spice level choice. Assigns integer response to confirmIndex
  let confirmIndex =
    input.keyInSelect(
      yesOrNo,
      "Ok, so you want your tacos to be " +
        spiceLevel[index] +
        ". Are you sure about this?"
    ) + 1;
  //Test
  // console.log(confirmIndex);

  return confirmIndex;
}

//Function passes integer result of 1 or 2 and displays resulting reply
function displayOrder(confirmIndex) {
  if (confirmIndex == 1)
    console.log("Okay, we will have your order right out.");
  else if (confirmIndex == 2) {
    console.log("What's the matter? Can't handle the heat?");
  } else {
    console.log("Your order has been cancelled.");
  }
  return 1;
}
