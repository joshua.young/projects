//Import Casual npm package and assign it
var casual = require("casual");

//Initialize needed variables and assign casual functions to them
let name = casual.name;
let country = casual.country;
let phone = casual.phone;
let address = casual.address;
let month = casual.month_name;

//Required statements with assigned variables in string literals
console.log(`Hello there ${name}`);
console.log(`How was your trip to ${country}`);
console.log(`I sure do hope you had a wonderful time.`);
console.log(`Is you phone number still ${phone}`);
console.log(
  `I will try to give you a call sometime. By the way, I want to send you a fresh loaf of bread at you address of: `
);
console.log(address);
console.log(
  `Well. I will see you soon. I will ve visiting sometime before ${month}.`
);
console.log("Until then, farewell");
