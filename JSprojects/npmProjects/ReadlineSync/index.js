//Import readline-sync and assigns to input
var input = require("readline-sync");

//Asks user for name
let username = input.question("What is your name? ");

//Asks user for favorite food
let favFood = input.question("What is your favorite food? ");

//Asks user for favorite drink
let favDrink = input.question("What is your favorite drink? ");

//displays user input in a sentence
console.log(
  `Hi ${username}, your favorite food is ${favFood} and your favorite drink is ${favDrink}.`
);
