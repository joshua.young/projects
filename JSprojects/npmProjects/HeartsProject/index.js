//Import and assign "ascii-heart" module to asciiHeart variable
var asciiHeart = require("ascii-heart");

//Call and log default asciiHeart function
console.log(asciiHeart());

//Call and log n0n-default asciiHeart function with a specifiedcharacter fill
console.log(asciiHeart(20, 20, { fill: "🇺🇸" }));

//Call and log n0n-default asciiHeart function with a specified character fill

console.log(asciiHeart(40, 40, { fill: "❤" }));
