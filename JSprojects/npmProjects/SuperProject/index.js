//Import Marvel-charcter npm package
var marvel = require("marvel-characters");

// logs a random marvel character
console.log(`Random Marvel Character: ${marvel()}`);

// lists all marvel characters
//console.log(marvel.characters);

//Displays the number of characters in the DB
let marvelDBlength = marvel.characters.length;
console.log(`# of characters in the marvel db: ${marvelDBlength}`);

//Creates new array by filtering marvel characters array and returning elements that start with "man"
//baselines element to lowercase to confirm
const manArray = marvel.characters.filter((el) => {
  return el.toLowerCase().startsWith("man");
});

//Finds and returns Iron Man if found
const starkArray = marvel.characters.find((el) => el == "Iron Man");

//Finds and returns Iron Man if found
const batArray = marvel.characters.find((el) => el == "Batman");

//Function checks if argument array is empty or not
function displayResult(arr) {
  if (arr === undefined || arr.length === 0) {
    return "Error: No element found with your search criteria";
  } else {
    return arr;
  }
}

//Displays the resulting array or error for "Man" Marvel Characters
console.log(
  `Results of "Man" filter on Marvel Character Array: ${displayResult(
    manArray
  )}`
);

//Displays result for search of "Iron Man"
console.log(
  `Your search for "Iron Man" resulted in:  ${displayResult(starkArray)}`
);

//Displays result for search of "Batman"
console.log(`Your search for "Batman" resulted in: ${displayResult(batArray)}`);
